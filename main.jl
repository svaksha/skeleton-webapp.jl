using Morsel
using Mustache      # For populating html templates with requested data
using HttpCommon    # For FileResponse(), which serves static files
using DataFrames    # For storing data in memory
using GLM           # For regression models
using JSON          # For converting julia dictionaries to JSON data

include("handlers.jl")

app = Morsel.app()

### Global variables
df = readtable("data/iris_data.csv")    # Raw data from csv, database, website, etc

### Serve static files directly
# See Morsel/Routes.jl for details as well as options for parameterising routes.
route(serve_static_files, app, GET, "/static/<level1>/<filename>")

### Dynamic content
route(home,         app, GET, "/home")
route(iris_model,   app, GET, "/model")
route(iris_2charts, app, GET, "/2charts")

### Run app (this is a blocking function)
start(app, 8000)
