This repo contains a simple self-contained web application written in Julia 
that data scientists can adapt to their own needs.

I am using it as a replacement for R's _shiny_ package in my own workflow, though it is far less mature and it 
differs in the details. The application is documented from the persepective of a data scientist (which I am) 
rather than a web developer (which I am not). The documentation is this README together with comments in the code.
Hopefully it is easily understandable.

The example application:

1. Reads data from a csv
2. Runs a linear regression
3. Produces some interactive charts that can be viewed in your browser

This basic pattern can be used, for example, to:

1. Read data from several sources (databases, websites, etc)
2. Spawn a new Julia process on [Google Compute Engine](http://www.blog.juliaferraioli.com/2013/12/julia-on-google-compute-engine.html) 
and run a simulation of the spread of an infectious disease
3. Display an animated map of the projected disease incidence over time

# Dependencies
The following Julia packages are required: Morsel, Mustache, HttpCommon, DataFrames, GLM, JSON.

# Usage
1. Clone this repo
2. `cd path/to/repo`
3. `julia main.jl`
4. In your browser go to `localhost:8000/home`


# How does it work? - A Crash Course in Web Development for Data Scientists

Consider a bar. A bar tender stands behind the bar and waits for a customer to order, fetches the requested items and returns them to the customer. Sometimes a requested item already exists, such as a bottle of beer, and can be served to the customer straight away. Other times the requested item needs to be built before it can be served, such as a cocktail. A web app is analogous to the bar environment. It has an address (the bar), a server (bar tender) and a set of resources (drinks) that can be requested. There are also clients (customers), though these are not part of the application. Finally there's you the programmer. You are the bar manager and therefore responsible for keeping things running smoothly.

To make a request a client sends a message called a request to the server. The request has a standard structure, including the server address and the resource location (think a particular fridge behind the bar). The address and location are placed together in one "word" called a URL, which is sufficient to determine which resource is being requested. For example, the URL _localhost:8000/home_ denotes a resource located at _home_ at the address _localhost:8000_. Actually in this example the address is _localhost_ and _8000_ is the port number, but we ignore this for now.

When these particular bar tenders are starting out they need to be told what to do. They can't figure it out for themselves but once they're told they're really good at serving. For example, we need to tell them that when a customer orders a cocktail, they need to locate all the ingredients, assemble them in the right way and serve up the drink. In web app jargon this process is called a _handler_ because it handles the request. In this application the mappings between a request for a particular resource and the handler to be used appear in `main.jl`. They have the form:

`route(handler, app, GET, resource_location)`

The handlers are implemented as functions in `handlers.jl`. Static files (bottled drinks) are non-changing and can therefore be served immediately. You will also see templates being populated with data. This is just mixing a drink with two ingredients, namely a template and data (which itself may require some preparation).

Now you should be able to follow what's happening in `main.jl` and `handlers.jl`. Please take a look and get back to me if my explanation is insufficient.

The bar analogy could be extended to explain reverse proxy servers, load balancing, worker processes and asynchronous event queues, but these concepts are overkill for our current purposes (and I have only rudimentary understanding of these things myself).

# Todo:
1. Suggestions for making the application more flexible/robust/scalable/secure 
are always welcome. Feel free to contribute!

2. The documentation merely reflects my understanding, some of which I have 
inferred, perhaps incorrectly. Please correct it where appropriate.

3. The charting library is [c3.js](http://c3js.org/), which provides many standard chart types that can 
be configured using JSON (i.e., server side using Julia code). See [here](http://c3js.org/examples.html)
for several common examples. I will add new chart types as my work demands it; 
for example, tables, maps, box plots, dashboards, etc. However, it is easy to add 
whatever templates, styling, javascript libraries and chart types you please.

4. Unlike R's _shiny_ package, the html templates are hard coded. For example, the 
application has a template with 2 tabs but not 3. To produce a template with 3 tabs 
simply modify the 2-tab template. I have no intention of programmatically producing 
html, but there is no reason an interested developer can't implement this capabilty 
(perhaps using [gumbo.jl](https://github.com/porterjamesj/Gumbo.jl)?).
