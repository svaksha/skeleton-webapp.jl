

function serve_static_files(req, res)
    # Serve static files directly
    # req::MeddleRequest
    filename = req.http_req.resource[2:end] # Omit leading "/"
    FileResponse(filename)
end


function home(req, res)
    ### Retrieve template
    tpl = open(readall, "templates/home.html")

    ### Set data
    tpl_data = {"home_text" => "Hi, this is the home page. Click the links to see some charts."}

    ### Return the template populated with data 
    Mustache.render(tpl, tpl_data)
end


function iris_model(req, res)
    ### Retrieve template
    tpl = open(readall, "templates/notabs_onechart.html")    # No tabs, 1 chart

    ### Generate data
    mdl = lm(PetalWidth ~ PetalLength, df)     # Linear regression model
    chart_spec = JSON.json(
                  {"bindto" => "#chart",       # Name of html element that the chart binds to
                   "data"   => {
                       "x"     => "x_data",    # Defines x axis data
                       "json"  => {
                           "x_data"      => df[:PetalLength],
                           "Petal Width" => df[:PetalWidth],
                           "Fitted"      => predict(mdl)},
                       "types" => {"Petal Width" => "scatter"}},    # Defaults to line chart
                   "axis" => {
                       "x" => {
                           "label" => "Petal Length",
                           "tick"  => {"fit" => false}},
                       "y" => {
                           "label" => {
                               "text"     => "Petal Width",
                               "position" => "outer-middle"}}}
                  })
    tpl_data = {"chart_spec" => chart_spec}

    ### Return the template populated with data 
    Mustache.render(tpl, tpl_data)
end


function iris_2charts(req, res)
    ### Retrieve template
    tpl = open(readall, "templates/twotabs.html")    # 2 tabs

    ### Generate data
    # Specify 1st chart
    chart1 = JSON.json(
                  {"bindto" => "#chart1",      # Name of html element that the chart binds to
                   "data"   => {
                       "xs" => {
                           "setosa"     => "x_setosa",    # Defines x axis data for each series
                           "versicolor" => "x_versicolor",
                           "virginica"  => "x_virginica"},
                       "json" => {
                           "x_setosa"     => df[df[:Species] .== "setosa",     :PetalLength],
                           "x_versicolor" => df[df[:Species] .== "versicolor", :PetalLength],
                           "x_virginica"  => df[df[:Species] .== "virginica",  :PetalLength],
                           "setosa"       => df[df[:Species] .== "setosa",     :PetalWidth],
                           "versicolor"   => df[df[:Species] .== "versicolor", :PetalWidth],
                           "virginica"    => df[df[:Species] .== "virginica",  :PetalWidth]},
                       "type" => "scatter"},
                   "axis" => {
                       "x" => {
                           "label" => "Petal Length",
                           "tick"  => {"fit" => false}},
                       "y" => {
                           "label" => {
                               "text"     => "Petal Width",
                               "position" => "outer-middle"}}}
                  })

    # Specify 2nd chart
    mean_petal_length = by(df, :Species, df -> mean(df[:PetalLength]))
    mean_petal_width  = by(df, :Species, df -> mean(df[:PetalWidth]))
    chart2 = JSON.json(
                  {"bindto" => "#chart2",      # Name of html element that the chart binds to
                   "data" => {
                       "json" => {
                           "Mean Petal Length" => mean_petal_length[:x1],
                           "Mean Petal Width"  => mean_petal_width[:x1]},
                       "type" => "bar"},
                       "bar"  => {"width" => {"ratio" => 0.5}},
                   "axis" => {
                       "x" => {
                          "type"       => "category",
                          "categories" => mean_petal_length[:Species]}}
                  })

    tpl_data = {"chart1" => chart1, "chart2" => chart2}

    ### Return the template populated with data 
    Mustache.render(tpl, tpl_data)
end
